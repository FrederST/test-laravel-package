<?php

namespace E4\Messaging;

use E4\Messaging\Exceptions\AMQPConnectionException;
use Exception;
use Illuminate\Support\Arr;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPSSLConnection;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AMQPConnection extends AMQPConnectionType
{
    private AMQPStreamConnection $connection;
    private AMQPChannel $channel;

    public function __construct()
    {
        parent::__construct(config('amqp'));
        $this->setConnection();
    }

    public function getConnection(): AMQPStreamConnection
    {
        return $this->connection;
    }

    public function getChannel(): AMQPChannel
    {
        return $this->channel;
    }

    public function shutdown(): void
    {
        $this->channel->close();
        $this->connection->close();
    }

    private function setConnection(): void
    {
        if (Arr::get($this->config, 'ssl', true)) {
            $this->connection = parent::getSSLConnection();
        } else {
            $this->connection = parent::getStreamConnection();
        }
        $this->connection->set_close_on_destruct(true);
        $this->channel = $this->connection->channel();
    }
}
