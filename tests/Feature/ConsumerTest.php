<?php

namespace E4\Messaging\Test\Feature;

use E4\Messaging\AMQPConnection;
use E4\Messaging\Facades\Messaging;
use E4\Messaging\Tests\TestCase;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Message\AMQPMessage;

class ConsumerTest extends TestCase
{
    private string $exchange = 'test_exchange';
    private string $queue = 'test_queue';

    public function test_consume_message()
    {
        $connection = new AMQPConnection();
        $channel = $connection->getChannel();
        $channel->queue_declare($this->queue, false, true, false, false);
        $channel->exchange_declare($this->exchange, AMQPExchangeType::DIRECT, false, true, false);
        $channel->queue_bind($this->queue, $this->exchange);
        $message = new AMQPMessage('data1');
        $channel->basic_publish($message, $this->exchange);
        Messaging::consume(function (AMQPMessage $message) {
            $this->assertEquals($message->body, 'data1');
            $message->ack();
        });
    }
}
