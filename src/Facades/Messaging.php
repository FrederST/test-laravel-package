<?php

namespace E4\Messaging\Facades;

use E4\Messaging\MessageBroker;
use Illuminate\Support\Facades\Facade;

class Messaging extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return new MessageBroker();
    }
}
