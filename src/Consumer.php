<?php

namespace E4\Messaging;

use Closure;
use Illuminate\Support\Arr;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;

class Consumer
{
    private bool $finish = false;
    private AMQPChannel $amqpChannel;
    private string $queue;

    public function __construct(AMQPChannel $amqpChannel)
    {
        $this->amqpChannel = $amqpChannel;
        $this->queue = Arr::get(config('amqp.queue'), 'name');
    }

    public function queue(string $queue): Consumer
    {
        $this->queue = $queue;
        return $this;
    }

    public function consume(Closure $closure): void
    {
        $this->amqpChannel
            ->basic_consume(
                $this->queue,
                '',
                false,
                false,
                false,
                false,
                function (AMQPMessage $message) use ($closure) {
                    $closure($message, $this);
                },
            );

        try {
            while ($this->amqpChannel->is_consuming() && false === $this->finish) {
                $this->amqpChannel->wait(null, false, 3);
            }
        } catch (\Exception $ex) {
            $this->amqpChannel->close();
        }
    }

    public function stop()
    {
        $this->finish = true;
    }
}
