<?php

namespace E4\Messaging\Test\Feature;

use E4\Messaging\AMQPConnection;
use E4\Messaging\Tests\TestCase;
use Illuminate\Support\Facades\Config;
use PhpAmqpLib\Exception\AMQPIOException;

class AMQPConnectionTest extends TestCase
{
    public function test_standard_connection()
    {
        $connection = new AMQPConnection();
        $this->assertNotNull($connection);
    }

    public function test_ssl_connection()
    {
        Config::set('amqp.ssl', true);
        $this->expectException(AMQPIOException::class);
        $connection = new AMQPConnection();
        $this->assertNotNull($connection);
    }

    public function test_shutdown_connection()
    {
        $connection = new AMQPConnection();
        $connection->shutdown();
        $this->assertFalse($connection->getConnection()->isConnected());
    }
}
