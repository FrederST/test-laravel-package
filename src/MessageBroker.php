<?php

namespace E4\Messaging;

use Closure;

class MessageBroker
{
    private AMQPConnection $amqpConnection;
    private Publisher $publisher;
    private Consumer $consumer;

    public function __construct()
    {
        $this->amqpConnection = new AMQPConnection();
        $this->publisher = new Publisher($this->amqpConnection->getChannel());
        $this->consumer = new Consumer($this->amqpConnection->getChannel());
    }

    public function publish(string $routingKey, array $message): void
    {
        $this->publisher->publish($routingKey, uniqid(), "", $message);
    }

    public function queue(string $queue): MessageBroker
    {
        $this->consumer->queue($queue);
        return $this;
    }

    public function consume(Closure $closure): void
    {
        $this->consumer->consume($closure);
    }
}
